# Models and Mappings

Note that models are **not required**; you could simply implement validation in
your Lambda functions in the programming language of your choice (as you would
for a traditional, non-serverless API for example). Models simply provide an
**easy, out-of-the-box solution** for validation.

You can also use models to **map** data. For example, in Integration Request
where we previously defined our Mapping Template, we can use our new Model
instead:

![use model for mapping](./screenshots/use-model-for-mapping.png)

Save the changes and test your function again. We can also use the model for
mapping the **Integration Response**.
