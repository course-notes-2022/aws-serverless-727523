# AWS Lambda: Useful Resources & Links

Just as with AWS API Gateway, we'll look into Lambda together. Maybe you still
want to browse the docs whilst following along or dive deeper into Lambda after
finishing this section. This is just a "Getting Started" course after all.

The following links should be helpful:

- AWS Lambda Overview: https://aws.amazon.com/lambda/

- AWS Lambda Developer Documentation:
  http://docs.aws.amazon.com/lambda/latest/dg/welcome.html

And of course, don't forget to think about the pricing!

Lambda charges you for executing code AND the duration each execution takes.
There's a generous free tier available though.

More details about Lambda pricing: https://aws.amazon.com/lambda/pricing/

## Improved Lambda Console

AWS enhanced the Lambda console but no worries, it still works as taught in the
course.

The features haven't changed, the setup process and UI just got modernized and
also a bit easier to use.

Here's how to create a function:

1. Click "Create function"

2. Leave the default selection (=> "Author from scratch")

3. Choose "Create a new role from template(s)" and enter any role name of your
   choice. You don't need to select anything in the "Policy templates" dropdown!
   Leave it empty.

4. Click "Create function"

Thereafter, you'll be in the Lambda editor UI where you can write code, attach
triggers (as shown in the next video => We won't attach any here) and where you
can edit the general Lambda config. Some items maybe be re-positioned (compared
to the video) but the general functionality is 100% the same.

5. Additionally, your Lambda function might have the async keyword added to it.
   Remove it to ensure that the code works as shown in this course!

The new console is cleaner and more powerful, writing code and testing it has
become easier. I strongly recommend watching the following presentation video
created by AWS where they introduce the new console and editor:
https://www.youtube.com/watch?v=pMyniSCOJdA

The new Lambda console/ editor part starts at 10:02:
https://youtu.be/pMyniSCOJdA?t=10m2s
