# Forwarding Requests with Proxy Integration

We can change data that reaches our Lambda function in the **Integration
Request** section of API Gateway. We can do this in one of two ways:

## Proxy Integration

1. In Integration Request > Method Execution > check "Use Lambda Proxy
   Integration" > OK
   - We are now passing the **complete** request data to our Lambda function,
     **not just the request body**.
2. In Lambda function handler definition, modify the function to: -
   ```javascript
   export const handler = (event, context, callback) => {
     callback(null, { headers: { 'Control-Access-Allow-Origin': '*' } });
   };
   ```
3. Deploy the function and re-test. Note that we don't receive any data in the
   `body`, but we can see our custom header and the request no longer fails with
   a `502`.
