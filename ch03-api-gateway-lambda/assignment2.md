# Assignment 2

1. **Create a new API** (e.g. "assignment-2")and **add a POST method** to it.

2. **Create a Model, feel free to re-use the Schema from before**. Optionally,
   if you feel super-confident, play around with the properties of the model and
   edit it.

3. Use the model to **validate incoming requests** (request bodies) AND **use it
   to create Body Mapping Templates** for both the request and response.

4. In the **request Body Mapping Template** you should ensure that **only ONE
   property reaches Lambda** (e.g. the `income` if you stick to the original
   model). In Lambda, use the value and return it (e.g. divide it by ten).

5. **In the response Body Mapping Template**, you should **re-construct your
   model** (i.e. add the two properties you dropped).

```
{
    "firstName": "John",
    "lastName": "Layton",
    "email": "jlayton@chiregister.com",
    "phone": "48J",
    "address": {
        "line1": "1001 Cottage Grove Ave",
        "line2": "",
        "city": "Heathville",
        "stateProvince": "CA",
        "zipPostal": "90001"
    }
}
```
