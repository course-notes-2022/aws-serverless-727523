# Understanding JSON Schemas

Models are defined using JSON schema. This might look complex at first but it
really isn't. It's actually pretty descriptive and ensures that we use clearly
defined models.

You can learn more about JSON Schema on the following page:
[http://json-schema.org/](http://json-schema.org/)

The **best place to start learning** is this page, though:
[https://spacetelescope.github.io/understanding-json-schema/](https://spacetelescope.github.io/understanding-json-schema/)
