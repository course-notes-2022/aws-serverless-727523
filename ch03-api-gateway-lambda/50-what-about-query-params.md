# What About Query Params?

In the last lecture, we had a look at Path Parameters (`/{type}` ). What about
Query Parameters (`?someParam=some-value` ) though?

No worries, we'll use these, too. Once we add authentication, we'll have a great
use case for passing data via query parameters. Until then, you can already keep
in mind that you can use them for validation (e.g. require certain query
parameters) and that you may extract them in Body Mapping Templates.
