# Extract Request Data with Body Mapping Templates

1. Integration Request > Mapping Templates > General Templates > select "Method
   Request Passthrough" > Save and Test

2. Inspect the returned/logged object. Note that we get something similar to the
   following:

```javascript
{
  "body-json": { "bandName": "Jimmy James and the Blue Flames" },
  "params": { "path": {}, "querystring": {}, "header": {} },
  "stage-variables": {},
  "context": {
    "account-id": "XXX",
    "api-id": "jlcehqp0g9",
    "api-key": "test-invoke-api-key",
    "authorizer-principal-id": "",
    "caller": "XXX",
    "cognito-authentication-provider": "",
    "cognito-authentication-type": "",
    "cognito-identity-id": "",
    "cognito-identity-pool-id": "",
    "http-method": "POST",
    "stage": "test-invoke-stage",
    "source-ip": "test-invoke-source-ip",
    "user": "XXX",
    "user-agent": "aws-internal/3 aws-sdk-java/1.12.397 Linux/5.4.228-141.415.amzn2int.x86_64 OpenJDK_64-Bit_Server_VM/25.362-b08 java/1.8.0_362 vendor/Oracle_Corporation cfg/retry-mode/standard",
    "user-arn": "arn:aws:iam::XXX:user/fakeUserXXX",
    "request-id": "XXX",
    "resource-id": "XXX",
    "resource-path": "/store-data"
  }
}
```

3. We can **narrow down** the data we're sending to the Lambda function to
   **only the request body** by \*\*removing everything other than `body-json`
   property in the template:

![request body json](./screenshots/req-body-json.png)

4. We can use the `$input.json` property to **further map** the object:

   ![input json](./screenshots/input.json.png)

   Testing the function with the following request body:

   ```
   {
       "person": {
           "firstName": "James",
           "lastName": "Hendrix",
           "age": 28
       }
   }
   ```

   Yields the following response:

   ```
   { "age": 28, "firstName": "James", "lastName": "Hendrix"}
   ```

   Also note that in the **CloudWatch logs**, we **receive _only_ the `person`
   object in the request**. We have successfully mapped our request object!
