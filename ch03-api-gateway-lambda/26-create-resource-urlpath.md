# Creating a Resource URL Path

1. Actions > Create Resource. Note that we are creating this resource on the
   **root** (`/`) resource.

2. Select root resource > Actions > Create resource > Add resource name. Note
   that we have two options:

   - Configure as proxy resource:

     > Proxy resources handle requests to all sub-resources using a greedy path
     > parameter, eg. {proxy+}. Creating a proxy resource also creates a special
     > HTTP method called ANY. The ANY method supports all valid HTTP verbs and
     > forwards requests to a single HTTP endpoint or Lambda integration.

   - Enable API Gateway CORS: Enables Cross-Origin Resource Sharing
