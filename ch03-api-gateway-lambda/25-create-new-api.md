# Create a New API

In this lecture we'll add a **new** API, for the course project we're building:

![api overview](../ch02-core-services/screenshots/app1-overview.png)

We'll begin with the POST method.

1. AWS Console > Services > API Gateway > Create New API > REST API > Build

   - Note that we have multiple **options** under "Create New API":

     - **New API**: create a brand new API from scratch

     - **Clone from existing API**

     - **Import from Swagger or Open API 3**

     - **Example API**

2. Add API name, optional description > Create

We'll add new **resources** next.
