# Intro

In this chapter, we'll focus on the core services we need for a serverless app:
API Gateway, Lambda, DynamoDB, and Cognito. We'll start with API Gateway.
