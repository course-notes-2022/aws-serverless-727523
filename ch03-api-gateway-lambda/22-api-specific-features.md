# API-Specific Features and Options

In the API Gateway console, we previously created our first API, consisting of a
**resource** and a **method**. A **resource** corresponds to a **URL path** or
route defined by our API (such as `/api/customers`). A **method** corresponds to
a **function/method** that responds to requests coming in for a specific
resource.

Note that we **must** DEPLOY before we can use our resources/methods live.

**Authorizers** allows us to add **authentication** to our API.

**Models** allows us to define the **shape** of the data our API uses.

**Documentation** allows us to create documentation for our API if we expect to
publish it for use by other developers.

**Dashboard** gives us logging for our API.
