# Lambda Pricing & Uploading Code

Want to play around with Lambda? It has a very generous free tier - learn more
here:
[https://aws.amazon.com/lambda/pricing/](https://aws.amazon.com/lambda/pricing/)

In the last lecture (and actually in the whole course) we used/ we'll be using
the In-line editor AWS Lambda provides. For shorter code snippets, that's
convenient.

But for more complex code (as well as my code which can be found attached to the
course lectures) you'll probably want to bundle all your code files into a ZIP
file and upload that (the course code already is attached as a
finished-to-be-uploaded ZIP file though!).

Here's how it works (for JavaScript, adjust the syntax if you're using another
language!):

1. Create a root entry file + handler method. For example an `index.js` file
   with the exports.handler = `(event, ...) => { ... }` method.

If you use a different file name AND/OR different starting handler function,
you'll need to adjust your Lambda config! Set Handler to
[FILENAME].[HANDLER-FUNCTION-NAME] (default: `index.handler`).

2. You may split your code over multiple files and import them into the root
   file via `require('file-path')` . This is also how you could include other
   third-party JavaScript (or other languages) packages.

3. Select all files and then zip them into an archive. Important: DON'T put them
   into a folder and zip the folder. This will not work!

4. Upload the created zip file to Lambda ("Code" => "Code entry type" => "Upload
   a .ZIP file")
