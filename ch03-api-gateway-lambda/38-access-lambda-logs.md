# Accessing Lambda Logs

We can inspect the event object by inspecting Services > CloudWatch > Logs.
Lambda stores its logs on CloudWatch.

Add a `console.log` statement to your Lambda function logging the `event`
object. Inspect the Cloud Watch logs and note that the object appears in the
logs for your Lambda function.

There is a more elegant way, which we'll see in future lessons.
