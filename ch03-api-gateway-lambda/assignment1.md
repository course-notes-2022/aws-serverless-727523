# Assignment 1: Create a New API

1. Create a new API via the API Gateway Console and give it any name you like
   (e.g. "assignment-1")

2. Create two resources on that newly created API

   /fetch-data /store-data

3. Add Http Methods to this endpoints

   /fetch-data GET Mock action /store-data POST Lambda action

The Mock endpoint should return some dummy/ static data (Hint: You learned how
to do that in the very first API we created in the first module of the course!

The Lambda action should simply execute its callback and return any data you
like

Don't forget to handle CORS!

Deploy the API and call it from a simple web app (like we used before)
