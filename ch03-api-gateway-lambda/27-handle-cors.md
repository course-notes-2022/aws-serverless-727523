# Handling CORS

CORS stands for **Cross-Origin Resource Sharing**. Generally it means that
applications in domain A **cannot access resources in domain B**. This is an
important web security feature.

However, when exposing an API we typically **want** applications in other
domains to access our API. How do we allow this? Our API server **must** return
a special **header** to the client (in this case the browser) that it is OK to
access resources on this server, otherwise the **browser will prohibit** these
cross-site actions. Also, modern browsers typically send **pre-flight requests**
to servers when sending a POST, PUT, DELETE requests. Pre-flight requests check
to see if the request is available and allowed. Your server needs to be able to
**handle** these requests, and provide an `OPTIONS` endpoint that returns the
right headers indicating that the CORS request is allowed.

We can check **Enable API Gateway CORS** to enable these requests, giving the
following output in the API Gateway console:

![enable api gateway cors](./screenshots/enable-apigateway-cors.png)

Inspecting `IntegrationResponse` reveals the following headers created for our
`OPTIONS` request:

![integration response](./screenshots/integration-response.png)
