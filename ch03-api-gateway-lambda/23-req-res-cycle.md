# Introducing the Request/Response Cycle

An **endpoint** is made up of the **resource** and the **method**, or type of
request (GET, POST, etc.).

The endpoint we defined in our first API defines a resource `/first-api-test`
that responds to `GET` requests. On the **right**, we have a depiction of the
**flow of data** in our API:

![api data flow](./screenshots/api-data-flow.png)

The **Method Request** box defines **how requests to this endpoint should
look**. We can click on this box to expose additional options:

![method options](./screenshots/method-options.png)
