# What is AWS Lambda?

It's finally time to run some code on receiving a request.

**AWS Lambda** allows you to host your code and **run it in response to some
event**. Events could include :

- File uploads to S3
- Scheduled CloudWatch events (cleanup jobs, etc)
- API Gateway (run code on HTTP requess)
- etc.

The event source kicks off code we store in Lambda. The function code must be
written in NodeJS, Python, Java, Go, Ruby, or C#. We can do any calculation,
interact woth other AWS services, and return a response.

![lambda and api gateway](./screenshots/lambda-and-apigateway.png)
