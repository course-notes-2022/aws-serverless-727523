# Using Path Parameters

We'll now add a `GET` route with a variable path. We _could_ create child
resources under the root resource, but instead we'll add a **variable**
resource:

1. API Gateway Console > Select your root resource > Actions > Add Resource >
   New Child Resource

2. Add resource name = "type" > resource path = '/customers/{type}':

   - Note that the `{}` allow us to add **path parameters**

3. Enable API CORS > Create Resource

4. Create a new Lambda function > Associate it with a new `GET` resource (if not
   already created) for the variable path.

5. Add a new mapping template to the **Integration Request**. We'll need this to
   parse the **path parameter** coming in on the request

![mapping params](./screenshots/mapping-params.png)

**Note that you _must_ enclose the value in double quotes!** This is necessary
for it to be parsed **as a string**.

Passing "all" as the `type` param should yield the expected results:

![mapping params](./screenshots/all-param-response.png)
