# Wrap-up

In this chapter, we learned:

- How to create API endpoints using **resources** and **methods**

- The API Gateway **request/response cycle**:
  - How to create a request schema, and ensure incoming requests conform to the
    schema
  - How to map incoming requests to Lambda functions
  - How to map Lambda return values
- **Stages**, or snapshots of our API that we **deploy**
- **Models**, optional for use in enforcing schema validation and data mapping
  templates
