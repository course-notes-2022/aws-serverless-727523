# Using Models and Validating Requests

We've learned a lot about body mapping templates to control the data that goes
**into** and comes **out of Lambda**. There's one other related concept to know.

**Models** allow us to define the structure of the data we want to work with in
our application.

1. API Gateway console > Models > Create > New Model > Add model name, content
   type (`application/json`), and description

2. Add model schema. Note that the schema must conform to JSON Schema language:

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "CompareData",
  "type": "object",
  "properties": {
    "age": { "type": "integer" },
    "height": { "type": "integer" },
    "income": { "type": "integer" }
  },
  "required": ["age", "height", "income"]
}
```

4. Create Model.

We can now use our model. One good place is on our incoming requests, to
validate that the incoming request body conforms to our model schema:

1. Method Request > Request Body > Add Model > Content type =
   `application/json` > model name = `CompareData` > request validator > save
   with check mark > Request Validator > Edit > Validate body > save with check
   mark

2. Test your function. Add the following **non-conforming** request:

```json
{
  "age": 39,
  "height": 60,
  "battingAvg": 0.5
}
```

Note that you should receive a `400` response with the body
`{"message": "Invalid request body"}`.

3. Test again with the following **conforming** request:

```json
{
  "age": 39,
  "height": 60,
  "income": 75000
}
```
