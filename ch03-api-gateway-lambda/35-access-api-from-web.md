# Accessing the API from the Web and Fixing CORS Issues

Let's now access our API from outside of API Gateway. For this we'll create a
simple script that sends a request to our API.

First we must **deploy** our API. We currently have no **stages** in our API
(resources are not live):

1. API Gateway console > select your API > Deploy API > create new stage >
   Deploy

2. Create a new CodePen:

```javascript
var xhr = new XMLHttpRequest();
xhr.open(
  'POST',
  'https://byzhsbfx5a.execute-api.us-east-1.amazonaws.com/dev/compare-yourself'
);
xhr.onreadystatechange = function (event) {
  console.log(event.target.response);
};
xhr.send();
```

3. Run the pen. Note that the response is `""`. This is because we have not set
   up our API Gateway to return `CORS` headers with the `POST` route.

4. In the API Gateway console, go to Resources > POST > Method Response > Add
   Response. Add the `Access-Control-Allow-Origin` header > Save.

5. Go to Integration Response > Add Integration Response > Header Mappings. Add
   `'*'` (with **single quotes**) as the Mapping value > Save.

6. Redeploy API: Actions > Deploy

7. Attempt to run the CodePen script again. You should now get a response:

```
"{'statusCode':200,'body':'\'Hello from Lambda!\''}"
```
