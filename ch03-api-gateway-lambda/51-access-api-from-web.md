# Accessing the API from the Web the RIGHT Way

In order to access our API endpoints from the web, we must do 2 things:

1. Enable CORS
2. Deploy the API

Note also that we now need to pass a **request object that corresponds to our
mapping** now that we've established mapping, as well as **make sure our
`Content-Type` header** is set to **`application/json`**.
