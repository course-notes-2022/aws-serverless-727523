# Understanding the Request/Response Cycle

**Integration Request** involves **extract/transforming incoming data** into the
required form for the **action** we are about to kick off, and passing it on to
the endpoint.

**Integration response** is kicked off once the **action is complete**. It
allows us to configure the **response** we are sending back.

**Method response** defines the **shape** of our response. We can configure
possible responses that we are sending back (headers, type of data, etc.).
