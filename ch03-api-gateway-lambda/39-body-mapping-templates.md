# Getting Started with Body Mapping Templates

Lambda functions should be **clean**, and only work with the data they **need**.
We should use API Gateway to ensure that we're only passing the data we need.

1. Go back to Integration Response > Use Lambda Proxy Integration. Uncheck the
   box > OK

2. In Lambda, refactor the handler to callback with the event. Test the request
   and note that we are back to the original state.

3. In Integration Request > Body Mapping Templates > When no templates defined >
   Content type = application/json > Generate template > add an empty object
   (`{}`) > Save

4. Test the function response in API Gateway again. Note the response `body` is
   an **empty object**.

> What we've done is **map** the response coming back from the Lambda function,
> **transforming it** (to an empty object in this case). In the next lesson,
> we'll learn how to do something more useful with body mapping!
