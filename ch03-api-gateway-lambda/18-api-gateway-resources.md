# API Gateway: Useful Resources & Links

We're going to dive into API Gateway together, no worries. But it's never too
early to share some useful links.

If you want to follow along or dive deeper into AWS API Gateway after finishing
this section, the following two links will be very helpful:

- API Gateway Overview: https://aws.amazon.com/api-gateway/

- API Gateway Developer Documentation:
  https://aws.amazon.com/documentation/apigateway/

Understanding the costs of API Gateway is also crucial. What you see in this
course will be within the free tier but once you start playing around with it on
your own or you're using it for production, you may encounter costs.

Check the following link to understand what's free and what's not:
https://aws.amazon.com/api-gateway/pricing/
