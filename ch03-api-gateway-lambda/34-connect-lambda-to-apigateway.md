# Connecting Lambda Functions to API Gateway Endpoints

In this lesson we'll connect the Lambda function we created in the previous
lesson to our `POST` route in API Gateway.

1. API Gateway console > Resources > Create new resource > `POST` > Integration
   type = Lambda Function

2. Add Lambda function **region** and **ARN** or name > Save > OK

3. Test > Test. You should receive output similar to the following:

**Response Body**:

```
{"statusCode":200,"body":"\"Hello from Lambda!\""}
```

**Response Headers**:

```
{"Content-Type":["application/json"],"X-Amzn-Trace-Id":["Root=1-63e8031c-0a693901054be537ed0b991a;Sampled=0"]}
```

**Logs**:

```
Execution log for request 5fbffe43-3e97-43ca-bda9-e52a161e42d7
Sat Feb 11 21:05:32 UTC 2023 : Starting execution for request: 5fbffe43-3e97-43ca-bda9-e52a161e42d7
Sat Feb 11 21:05:32 UTC 2023 : HTTP Method: POST, Resource Path: /compare-yourself
Sat Feb 11 21:05:32 UTC 2023 : Method request path: {}
Sat Feb 11 21:05:32 UTC 2023 : Method request query string: {}
Sat Feb 11 21:05:32 UTC 2023 : Method request headers: {}
Sat Feb 11 21:05:32 UTC 2023 : Method request body before transformations:
Sat Feb 11 21:05:32 UTC 2023 : Endpoint request URI: https://lambda.us-east-1.amazonaws.com/2015-03-31/functions/arn:aws:lambda:us-east-1:815338849297:function:cy-store-data/invocations
Sat Feb 11 21:05:32 UTC 2023 : Endpoint request headers: {x-amzn-lambda-integration-tag=5fbffe43-3e97-43ca-bda9-e52a161e42d7, Authorization=***********************************************************************************************************************************************************************************************************************************************************************************************************d7755e, X-Amz-Date=20230211T210532Z, x-amzn-apigateway-api-id=byzhsbfx5a, X-Amz-Source-Arn=arn:aws:execute-api:us-east-1:815338849297:byzhsbfx5a/test-invoke-stage/POST/compare-yourself, Accept=application/json, User-Agent=AmazonAPIGateway_byzhsbfx5a, X-Amz-Security-Token=IQoJb3JpZ2luX2VjED0aCXVzLWVhc3QtMSJHMEUCIHsFOA64Q20FnW9LnuHssJFv/DWTN4KFBvhfXe2AkAugAiEA9ZfG5Q7dZXa01KfEUN4x+S3+1iwER+E6hBHSI2TP5n8q1gQIxf//////////ARAAGgwzOTIyMjA1NzY2NTAiDATFlQadQIYjHezmHSqqBPpv7KOU/QxyjJUhaVmHrMKMAhTZqTGYoSVLv2pkf+IR4ikF7rSRrrORvS+irLj6O+T5prfJ726SytiMyDntzLuYnZlHH7m3ZVZsAks8KmKjuVovc594s255QZjP4EjmWz8cEoZpcBydUusT6rUhtcMH [TRUNCATED]
Sat Feb 11 21:05:32 UTC 2023 : Endpoint request body after transformations:
Sat Feb 11 21:05:32 UTC 2023 : Sending request to https://lambda.us-east-1.amazonaws.com/2015-03-31/functions/arn:aws:lambda:us-east-1:815338849297:function:cy-store-data/invocations
Sat Feb 11 21:05:32 UTC 2023 : Received response. Status: 200, Integration latency: 360 ms
Sat Feb 11 21:05:32 UTC 2023 : Endpoint response headers: {Date=Sat, 11 Feb 2023 21:05:32 GMT, Content-Type=application/json, Content-Length=50, Connection=keep-alive, x-amzn-RequestId=ddac98ed-c7c1-4ba8-bd34-a358e29e7df6, x-amzn-Remapped-Content-Length=0, X-Amz-Executed-Version=$LATEST, X-Amzn-Trace-Id=root=1-63e8031c-0a693901054be537ed0b991a;sampled=0}
Sat Feb 11 21:05:32 UTC 2023 : Endpoint response body before transformations: {"statusCode":200,"body":"\"Hello from Lambda!\""}
Sat Feb 11 21:05:32 UTC 2023 : Method response body after transformations: {"statusCode":200,"body":"\"Hello from Lambda!\""}
Sat Feb 11 21:05:32 UTC 2023 : Method response headers: {X-Amzn-Trace-Id=Root=1-63e8031c-0a693901054be537ed0b991a;Sampled=0, Content-Type=application/json}
Sat Feb 11 21:05:32 UTC 2023 : Successfully completed execution
Sat Feb 11 21:05:32 UTC 2023 : Method completed with status: 200

```
