# What's the Idea Behind Body Mappings?

Body mappings allow us to get **only** the data we **need** into Lambda. The
body mapping template uses the `$input` refers to your **request data**. The
`$input.json()` method allows us to **extract JSON from the request**, and the
`$` passed to `json` (e.g. `$input.json($.person.age)`) refers to everything in
the **request body**. We can use dot notation to get properties on the request
body.

In this way, our Lambda functions **don't have to care** about the structure of
the request coming in to API Gateway, and API Gateway can **tell Lambda what
structure to expect and use**.

We can also use mappings in **Integration Response**. We'll learn how in the
next lecture.
