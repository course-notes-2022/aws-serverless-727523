# Understanding `event` in Lambda Functions

Now let's learn how to work with **data** in our Lambda functions. Testing the
`POST` route we created for our assignment 1 solution, we can pass the following
in the request `body`:

![pass event](./screenshots/pass-event.png)

If we modify our Lambda function that is handling the request to return the
`event` object, we see our data we passed in the request:

![pass event response](./screenshots/pass-event-response.png)
