# Mapping Response Data

Let's use body mapping in the Integration Response to transform the **response
we get back from Lambda**.

1. Integration Response > Click Caret > Body Mapping Templates

   - Note that an **empty** template simply **forwards the data** from Lambda to
     the client. We can change this by adding a **mapping object**. For example,
     adding `{}` will return an empty object, regardless of what Lambda returns.

2. Add the following mapping template to parse the `age` property from the
   response object onto the property `personAge` of the template:

   ```javascript
   {
       "personAge": $input.json('$.age')
   }
   ```

3. Save and test. Note that the response should be:
   ```json
   { "personAge": 28 }
   ```
