# General API Gateway Features

Let's take a look at the **options** that are available to us in API Gateway:

![api gateway options](./screenshots/api-gateway-options.png)

## Usage Plans & API Keys

For these two options it's important to understand **how** your API will be
used:

- In a **proprietary** app?
- As a **publicly-available** API?

In a **proprietary app**, API keys aren't really necessary as **our own app is
the only client** of our API. Others can't do much with the links to our API
(especially if we **protect** our endpoints with user authentication).

For **publicly-available** APIs that we expose for use by **other developers**,
API keys are important (e.g. Goggle Maps API). Users must pass an API key that
we generate in order to access our API. We can limit usage, deny access, etc.
using API keys and Usage plans.

## Custom Domain Name

Custom domain names allow us to expose our API via a domain name that we own.

## Client Certificates

These are important if we plan to **forward requests** received on an endpoint
to **another endpoint**. The certificate verifies that the request came from a
**trusted, recognized** endpoint source.

## Settings

The settings option allows us to manage the roles and permissions that our
service has within our AWS account (e.g. permissions to generate logs, etc.).
