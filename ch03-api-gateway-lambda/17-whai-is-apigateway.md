# What is API Gateway?

API Gateway is an AWS service that allows us to **define API endpoints and HTTP
methods**. We can also **authorize access** to the API endpoints, as well as
associate **actions** (e.g. run Lambda code) with the endpoints and methods:

![api gateway](./screenshots/what-is-apigateway.png)
