# AWS: DynamoDB Useful Resources & Links

As always, we'll have a closer look at DynamoDB in this module. But in case that
you want to read along or dive deeper into some of the concepts I touch on, the
following links should be very helpful:

- Overview over DynamoDB:
  [https://aws.amazon.com/dynamodb/](https://aws.amazon.com/dynamodb/)

- Developer Documenation:
  [http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html)

And, as always - don't forget to check what it costs! It's mostly about
provisioned capacity units, more info can be found in the pricing article:
[https://aws.amazon.com/dynamodb/pricing/](https://aws.amazon.com/dynamodb/pricing/)
