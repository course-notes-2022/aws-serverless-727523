# NoSQL vs SQL

| NoSQL                              | SQL (Relational)                     |
| ---------------------------------- | ------------------------------------ |
| No relations                       | Relations                            |
| High flexibility (No/Weak Schemas) | Limited Flexibility (Strong Schemas) |
| Data repetition                    | No Data Repetition                   |
| No integrity checks                | Integrity checks                     |
| Easy scalability                   | Harder scalability                   |
