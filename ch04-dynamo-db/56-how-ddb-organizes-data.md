# How DynamoDB Organizes Data

## Keys, Attributes, and Indices

In every DDB table, we're **required** to have a **Partition Key**. The
partition key is an attribute of the objects stored in the table, e.g. `userId`.
Objects can have as many additional attributes as necessary:

![partition key](./screenshots/partition-key.png)

The partition key **must** be unique, and present on **each item** in the table.
The partition key functions as the **primary key** for data in the table.

DDB allows you to create a **Sort Key** as well. You can optionally **combine**
the partition key and the sort key, and use that composite as the primary key
for the table. In this case, the **combination** of the partition and sort key
must be unique.

You can set up **global secondary indices** to make querying on the index more
efficient.

![keys and indices](./screenshots/keys-indices.png)
