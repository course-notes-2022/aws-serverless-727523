# What is Dynamo DB?

**DynamoDB** is a fully-managed, NoSQL database offered by AWS. In a NoSQL
database, there are **no relations** between tables. DDB is a great option for
serverless apps because you don't need to manage **servers** (provisioning,
scaling etc.).
