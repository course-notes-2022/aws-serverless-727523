# Create a Table in DynamoDB

1. AWS Management Console > Services > DynamoDB > Create Table

2. Add table name > Add partition key > Default settings (note: customizable) >
   Create

## Provisioned Capacity

As part of the configuration (and according to DDB's pricing model) you
**configure** the **read/write capacity units**, that is, you tell DDB how many
**reads and writes per second** you intend your database to be able to perform.
