# Using DynamoDB with Lambda

DDB can be both an event source for kicking off Lambda functions, but it can
also be the Lambda function's **data repository**.
