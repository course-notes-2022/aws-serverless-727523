# Intro

In this chapter, we'll learn how to integrate our API with **DynamoDB** for data
persistence.
