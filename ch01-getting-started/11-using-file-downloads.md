# How to use the File Downloads

In this course, I provide the API we create and the code we write for download
(the API as a definition file). Typically, the files for a module are attached
to the last lecture of that module! API Definition Files:

API Snapshots are provided as .json definition files (using Swagger + API
Gateway Extensions). You can import them directly in API Gateway. Simply create
a new API and choose "Import from Swagger".

## BEFORE IMPORTING:

Replace the **REGION** placeholder (in the .json file) with the region where
your Lambda functions are stored, replace the **ACCOUNT_ID** placeholder with
your account id. Your Lambda function names have to match mine, otherwise you
have to adjust these names in the .json files, too! AWS Lambda ZIP Files:

All Lambda code is provided as ZIP files. You can easily use these by creating a
new function (with "blank" template) and choosing "Upload a .ZIP file" from the
"Code entry type" dropdown.
