# First Serverless API

In this lesson, we'll create our first API using API Gateway.

1. AWS Management Console > Services > API Gateway > REST API > Build > New API
2. Add a name and optional description for your API. Select an **Endpoint
   Type** > Create API
3. Actions > Create Resource > Add Resource Name > Create Resource
4. Actions > Create Method > GET > Select "Mock" > Save
5. Click on "Integration Response" > Add Integration Response > click down
   caret > Mapping Templates > Add mapping template > input "application/json" >
   Add a JSON-formatted response object > Save
6. Actions > Deploy API > Deployment stage > New Stage > Add stage name > Deploy
7. Copy "Invoke URL" > open new tab in browser > append API name selected in
   step 2 to URL
