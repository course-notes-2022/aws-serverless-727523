# Intro: Core AWS Serverless Services

In this section, we'll dive deeper into the core AWS services we'll need to
build with a serverless architecture.
