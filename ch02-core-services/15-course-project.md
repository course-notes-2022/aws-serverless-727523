# Course Project: "Compare Yourself" App

In this project, we'll use a (pre-built) frontend app and build an API to add
authentication to the frontend app. We will host it on S3. We will define 3
endpoints for our API that are protected, and execute Lambda functions whenever
authtenticated requests reach our endpoints. We will store customer data in
DynamoDB, and use CloudFront for caching.

![app 1 overview](./screenshots/app1-overview.png)
