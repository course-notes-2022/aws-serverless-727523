# Overview of Core Serverless Services

## Which Services do we Need?

Consider a **static web app** (e.g. an Angular app):

1. S3: used for hosting the Angular app; simply store files and configure to be
   accessible from the web

2. API Gateway: service to create a REST API with different routes and request
   handlers

3. AWS Lambda: execute code (functions) on demand

4. DynamoDB: a no-SQL database solution that does not require us to
   manage/maintain database servers

5. Cognito: easily create user authentication (e.g. protect REST API from
   unauthenticated users)

6. Route 53: register and configure a domain to load our (S3-hosted) app to a
   domain name

7. CloudFront: Caching
